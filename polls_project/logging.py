from rich import print


class OperationLogger:
    def log(self, operation, args, results):
        msg = (
            f"[green]Operation:[/green] {operation} "
            f"Arguments: [green]{args}[/green] Results: {results}"
        )
        print(msg)
