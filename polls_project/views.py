from django.http import HttpResponse
from django.shortcuts import redirect, render

from .forms import PostForm
from .models import Post


def index(request):
    return HttpResponse("Hello, world.")


def post_list(request):
    posts = Post.objects.all()
    return render(request, "post_list.html", {"posts": posts})


def post_create(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("post_list")
    else:
        form = PostForm()
    return render(request, "post_create.html", {"form": form})
