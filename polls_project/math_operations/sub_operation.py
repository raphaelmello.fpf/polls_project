from polls_project.math_operations.operations import Operation


class SubOperation(Operation):
    def compute(self, first_operand, second_operand):
        return self.sub(first_operand, second_operand)

    def sub(self, a, b):
        return a - b
