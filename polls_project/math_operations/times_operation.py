from .operations import Operation

class TimesOperation(Operation):
    def compute(self, first_operand, second_operand):
        return self.times(first_operand, second_operand)

    def times(self, a, b):
        return a * b
